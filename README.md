### BATTLE OF HOTH

[play BATTLE OF HOTH](http://s797515133.websitehome.co.uk/harrier_carrier_website/battleofhoth/)   

![screenshot.png](https://bitbucket.org/repo/jX7Lxg/images/1194364849-screenshot.png)
 
## What is BATTLE OF HOTH?
* HTML5 game built in Javascript using [Phaser game Library](http://phaser.io) and using an [isometric plugin](http://rotates.org/phaser/iso/) 
* Based on **LEGO set 3866 Star Wars: The Battle of Hoth**
* **[gameplay video - 60 secs - 1 full game](https://www.youtube.com/watch?v=9QmqRVbC9Os)**
* **[gameplay video - 7 secs Vine fast-action cut](https://www.youtube.com/watch?v=u66W9E0Eh-I)**
* ![3866.jpg](https://bitbucket.org/repo/jX7Lxg/images/2263598536-3866.jpg)
* It currently has 6 different game boards, with different piece combinations
* I [photographed](http://s797515133.websitehome.co.uk/harrier_carrier_website/battleofhoth/assets/images/photoshoot.jpg) the LEGO set to product a sprite sheet for use with the game: [sprite PNG](http://www.battleofhoth.com/assets/images/hoth.png)

## Assets
* ![](https://bitbucket.org/wizzard262/battleofhoth/raw/d4e6ddbf164d5a4d8fcb9a6b026bc6e26b85178a/assets/font/fontBlue.png)
* ![](https://bitbucket.org/wizzard262/battleofhoth/raw/d4e6ddbf164d5a4d8fcb9a6b026bc6e26b85178a/assets/images/fire.png)
* ![](https://bitbucket.org/wizzard262/battleofhoth/raw/d4e6ddbf164d5a4d8fcb9a6b026bc6e26b85178a/assets/images/hoth.png)
* ![](https://bitbucket.org/wizzard262/battleofhoth/raw/d4e6ddbf164d5a4d8fcb9a6b026bc6e26b85178a/assets/images/loading.gif)
* ![](https://bitbucket.org/wizzard262/battleofhoth/raw/d4e6ddbf164d5a4d8fcb9a6b026bc6e26b85178a/assets/images/snow.jpg)
* ![](https://bitbucket.org/wizzard262/battleofhoth/raw/d4e6ddbf164d5a4d8fcb9a6b026bc6e26b85178a/assets/images/snowflakes.png)

## How do I play?
* **[This video shows how to play the lego star wars 3866 board game](https://www.youtube.com/watch?v=l0QGaV-b4cw)**
* The game should work in the browser on most devices
* **2 player turn-based board game** similar to chess (no A.I. to play against yet)
* Empire begins
* **Click / touch to select one of your pieces**
* **You can MOVE or SHOOT.** 
* The spaces the piece can move are shown as green grid squares (see screenshot). All pieces can onyl move 1 space where the map grid allows, or no other peices are present.
* The spaces you can fire upon are shown as red grid squares (see screenshot) and are shown on the pieces info box at bottom right. Different pieces have different firing ranges.
* **Each TURN consists of 2 ACTIONS which can be MOVE or SHOOT** with any of your pieces. The current move is indicated by the faction badges at top right.
* **SHOTS HIT 66.6% of the time and MISS 33.3%** 
* A successful shot will take 1 health from the attacked piece, and a figure will be lost from the piece ( or if vehicle is empty of figures it is destroyed)
* Each piece has health of number of figures (+ 1 for vehicle if present), as inidicated in bottom left info panel
* Each piece's health is the sum of its micro-figures, plus 1 for any vehicle in that piece.
* **Darth Vader and Luke Skywalkers units have a SPECIAL FORCE MOVE** 33.3% of the time, destroys all heath of an attacked piece.
* **Destroy all opposition or cross the game board to WIN!**

## How do run the source code locally
* The files are all client-side, HTML and javascript.
* Everything runs from the index.html file in the root
* It must be run under a webserver (not off the file system)

## Prototype

[My first commit shows the game basics executed](http://www.battleofhoth.com.gridhosted.co.uk/prototype/index.html)

---

[see also Harrier Carrier: Cold War](https://bitbucket.org/wizzard262/harrier_carrier_website/)

* ![harriercarrier](http://www.harriercarrier.com.gridhosted.co.uk/assets/screenshot1.png)


